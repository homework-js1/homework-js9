const arr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
const arr2 = ["1", "2", "3", "sea", "user", 23];
const parent = document.body;


function createList(array, parent){
    const list = document.createElement('ol');
    array.map((array) => {
        const listItem = document.createElement('li');
        listItem.append(array);
        list.append(listItem);
    });
    parent.append(list);


    return array;
}

let res = createList(arr2, parent);

setTimeout(() =>{
    parent.remove();
    }, 3000);
